#  TOROS Image Analysis and Machine Learning algorithms for transient detection.


This repository will contain python scripts and 
ipython notebooks with tools to do image analysis.

External libraries required:
----------------------------

[Open-CV](http://opencv.org/downloads.html)
[Astropy](http://www.astropy.org)
[Photutils](https://github.com/astropy/photutils)

Martin Beroiz - University of Texas at Brownsville / University of Texas at San Antonio

email: <martinberoiz@phys.utb.edu>

Version 0.3.1

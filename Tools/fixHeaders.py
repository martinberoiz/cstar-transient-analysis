"""
    # fixHeaders.py
    
    This tool will fix the headers for a CSTAR image or a coaddition of CSTAR images.
    * Copy the WCS of a FITS file provided in the wcs option.
    * Do the photometry with a provided catalog file in the cat option and add the MAGZERO header keyword.
    * Changes the date format (a string in CSTAR) to ISOT format for header keyword DATE-OBS.
    
    Usage:
    python fixHeaders [options args] rootDir
    Options:
    -v, --version: Prints the version of this program.
    -h, --help:    Prints this page.
    -w, --wcs:     Supply the FITS file with the WCS in the header.
    -c, --cat:     Supply the catalog of stars to be used in photometry.
    rootDir:       Set the root directory that contains all the FITS files to be modified.
 
    Martin Beroiz - June 2014
    <martinberoiz@phys.utb.edu>
    University of Texas at San Antonio
"""

import os
import getopt
import sys
from astropy.io import fits
from astropy.io import ascii
from astropy import wcs
from astropy.time import Time
import datetime as d
import numpy as np
import math
from photutils import CircularAperture, aperture_photometry

__version__ = '0.1'

def fixHeader(hdulist, refHeaderFile, catalogFileName):
    """Call the functions that actually solve individual issues. addWCS, correctDate and photoCalibrate."""
    modified = False
    head = hdulist[0].header
    if 'WCSAXES' not in head.keys():
        addWCS(hdulist, refHeaderFile)
        modified = True

    #This checks if the month is written like 'Jun'
    datestring = head['date-obs']
    if datestring.strip()[5:8].isalpha():
        correctDate(hdulist)
        modified = True

    #if 'MAGZERO' not in head.keys():
    try:
        photoCalibrate(hdulist, catalogFileName)
        modified = True
    except:
        print("Problem doing photometric calibration. Calibration not done.")
        pass

    return modified


def addWCS(hdulist, refHeaderFile):
    """Copy the WCS info from the reference file into the header."""
    head0 = fits.getheader(refHeaderFile)
    head1 = hdulist[0].header
    
    for akey in head0.keys()[11:]:
        if akey != 'comment'.upper() and akey != 'history'.upper() and akey != 'date'.upper():
            head1[akey] = (head0[akey], head0.comments[akey])


def correctDate(hdulist):
    """Correct the CSTAR date format (a string) for the DATE-OBS header keyword and replace with an ISOT format."""
    head = hdulist[0].header
    
    dateStr = head['DATE-OBS'].strip()
    timeStr = head['TIME'].strip()
    t = d.datetime.strptime(" ". join([dateStr, timeStr]), '%Y %b %d %H:%M:%S.%f')
    #change to UTC by subtracting 5 hours:
    t -= d.timedelta(hours = 5)
    
    #Create an astropy time to correct using the polynomials
    thead = Time(t.strftime('%Y-%m-%d %H:%M:%S.%f'), format='iso', scale='utc')
    
    #2455391.5 corresponds to the UTC date 2010 Jul 14 00:00:00
    dt = thead.jd - 2455391.5
    
    #corr is the correction in seconds
    if dt < 0:
        corr = 1.70607*dt + 4.39088e-2*dt**2 + 7.60477e-4*dt**3 + 4.36212e-6*dt**4
    else:
        corr = 1.17962*dt - 2.74530e-2*dt**2 + 5.65247e-4*dt**3 - 2.98125e-6*dt**4
    
    #change corr from seconds to days
    corr /= 86400
    
    #tcorr is the corrected time by adding corr in days
    tcorr = Time(thead.jd + corr, scale='utc', format='jd')
    
    head['date-obs'] = (tcorr.isot, 'Julian UTC datetime with correction')
    del head['time']


def photoCalibrate(hdulist, catalogFileName):
    """Calibrate photometrically the FITS file by calculating and adding the MAGZERO keyword to the header."""
    img = hdulist[0].data
    img_mask = hdulist[1].data.astype('bool')
    
    #get ra, dec from catalog file
    data = ascii.read(catalogFileName)
    ra_dec = np.array([[aline[0], aline[1]] for aline in data])
    mags = np.array([aline[2] for aline in data])
    
    head = hdulist[0].header
    theWCS = wcs.WCS(head)
    
    apR = 3.5
    
    mm, ss = bkgNoiseSigma(img, noiseLvl = 3.0, goodPixMask = img_mask)
    fluxes, gmask = getFluxesForRADec(ra_dec, img, img_mask, theWCS, bkg_level=mm, apRadius=apR)
    mags = mags[gmask]
    
    #Remove flux too small
    minflux = ss * math.pi * apR**2
    mags    = mags[fluxes > 2*minflux]
    fluxes  = fluxes[fluxes > 2*minflux]
    
    best_intercept = sigmaClipIntercepFit(mags, np.log10(fluxes), nsig = 3, a = -0.4)
    
    head['magzero'] = (2.5*best_intercept, 'Added by Martin Beroiz.')
    
    return


def getFluxesForRADec(radecList, image, goodMask, the_wcs, bkg_level = 0, apRadius = 3.5):
    """Get the aperture flux at the (RA, Dec) position for the radecList array"""
    pix_ctr = []
    good_pix_mask = []
    for aPoint in radecList:
        try:
            pixPoint = the_wcs.all_world2pix([aPoint],0,tolerance=1E-3)[0]
            x,y = pixPoint.astype('int')
            h, w = image.shape
            if(x > w or x < 0 or y < 0 or y > h):
                good_pix_mask.append(False)
            elif(goodMask[y,x] == False):
                good_pix_mask.append(False)
            else:
                pix_ctr.append(pixPoint)
                good_pix_mask.append(True)
        except:
            good_pix_mask.append(False)
    
    pix_ctr = np.array(pix_ctr)
    good_pix_mask = np.array(good_pix_mask)
    
    apertures = CircularAperture(pix_ctr, apRadius)
    fluxes = aperture_photometry(image, apertures)
    fluxes -= bkg_level * np.pi * apRadius**2
    
    return fluxes, good_pix_mask


def sigmaClipIntercepFit(x, y, nsig = 3, a = -2.5):
    """Return the best fit to the y-intercept of the x,y data assuming a is fixed, ignoring outliers.
        
    This is a least square problem to solve the equation y=ax+b when a is fixed (not fitted)
    to some value. It will also clip out recursively data points that are away from nsig std dev
    from the current iteration.
    It stops when it doesn't find any more outliers away from nsig std. deviations.
    Return b
    """
    b = -sum((a*x - y))/len(x)
    sig = math.sqrt(sum((x*a + b - y)**2)/len(x))
    gpts_mask = np.abs(x*a + b - y) < nsig*sig
    
    gx = x.copy()
    gy = y.copy()
    MAX_ITER = 1000
    i = 0
    while(sum(gpts_mask) != len(gx) and i < MAX_ITER):
        gx = gx[gpts_mask]
        gy = gy[gpts_mask]
        b = -sum((a*gx - gy))/len(gx)
        sig = math.sqrt(sum((gx*a + b - gy)**2)/len(gx))
        gpts_mask = np.abs(gx*a + b - gy) < nsig*sig
        i += 1
    return b


def bkgNoiseSigma(dataImg, noiseLvl = 3.0, goodPixMask = None):
    """Return background mean and std. dev. of sky background.
        
        Calculate the background (sky) mean value and a measure of its standard deviation.
        Background is considered anything below 'noiseLvl' sigmas.
        goodPixMask is a mask containing the good pixels that should be considered.
        Return mean, std_dev
        """
    m = dataImg[goodPixMask].mean()
    s = dataImg[goodPixMask].std()
    
    prevSgm = 2*s #This will make the first while condition true
    tol = 1E-2
    while abs(prevSgm - s)/s > tol:
        prevSgm = s
        bkgMask = np.logical_and(dataImg < m + noiseLvl*s, dataImg > m - noiseLvl*s)
        if goodPixMask != None: bkgMask = np.logical_and(bkgMask, goodPixMask)
        m = dataImg[bkgMask].mean()
        s = dataImg[bkgMask].std()
    
    return (m, s)


def scanThruFiles(init_path, refHeaderFile, catalogFileName):
    """Scan through init_path and call fixHeader on each FITS file found."""
    listing = os.listdir(init_path)
    listing = [os.path.join(init_path, anitem) for anitem in listing
               if anitem.endswith('.fits') or os.path.isdir(os.path.join(init_path, anitem))]
    listing.sort()
    for infile in listing:
        if os.path.isdir(infile):
            print('Getting into folder: %s' % (os.path.basename(infile)))
            scanThruFiles(infile, refHeaderFile, catalogFileName)
        else:
            #Do work here
            print('working with file %s' % (os.path.basename(infile)))
            hdulist = fits.open(infile)
            wasMod = fixHeader(hdulist, refHeaderFile, catalogFileName)
            if wasMod: hdulist.writeto(infile, clobber = True)
            hdulist.close()


if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvw:c:", ["help","version","wcs=","cat="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(__doc__)
            sys.exit(2)
        if opt in ('-v', '--version'):
            print('Version: ' + __version__)
            sys.exit(2)
        if opt in ('-w', '--wcs'):
            refHeaderFile = arg
        if opt in ('-c', '--cat'):
            catalog = arg

    root_dir = args[0]
    scanThruFiles(root_dir, refHeaderFile, catalog)


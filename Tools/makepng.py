#!/usr/local/Cellar/python/2.7.5/bin/python

"""  
  # makepng.py
  ## TorosProject

  Created by Martin Beroiz on 11/20/13.
  Copyright (c) 2013 CGWA. All rights reserved.
  email: <martinberoiz@phys.utb.edu>

  This script will create a png image of the fits file
  without any processing.
"""

from matplotlib import pyplot as plt
from astropy.io import fits
import sys
import os

for ffile in sys.argv[1:]:
    hdulist = fits.open(ffile)
    scidata = hdulist[0].data
    hdulist.close()

    plt.figure(figsize=(8,6), dpi=80)
    plt.imshow(scidata,cmap='binary_r')
    plt.colorbar(shrink=.92)

    fname = os.path.basename(ffile)
    fulldir = os.path.dirname(ffile)
    name, ext = os.path.splitext(fname)

    outbname = name + '_img.png'
    outFullPath = os.path.join(fulldir,'pngs/',outbname)
    plt.savefig(outFullPath, dpi=72)


""" 
    TOROS Processing Tools - Image Toolbox
    --------------------------------------
    
    This set of image tools are intended to process astronomical images
    for the Transient Optical Robotic Observatory of the South (TOROS).
    Tested with CSTAR images.
    
    Martin Beroiz - 2014
    <martinberoiz@phys.utb.edu>
    University of Texas at San Antonio
"""

import numpy as np
import cv2
from astropy.io import fits
import os
import datetime as d
import ransac
import math

__version__ = '0.3.1'

################################################################
# CSTAR IMAGE PROCESSING ---------------------------------------

def bkgNoiseSigma(dataImg, noiseLvl = 3.0, goodPixMask = None):
    """Return background mean and std. dev. of sky background.
        
    Calculate the background (sky) mean value and a measure of its standard deviation.
    Background is considered anything below 'noiseLvl' sigmas.
    goodPixMask is a mask containing the good pixels that should be considered.
    Return mean, std_dev
    """
    m = dataImg[goodPixMask].mean()
    s = dataImg[goodPixMask].std()
    
    prevSgm = 2*s #This will make the first while condition true
    tol = 1E-2
    while abs(prevSgm - s)/s > tol:
        prevSgm = s
        bkgMask = np.logical_and(dataImg < m + noiseLvl*s, dataImg > m - noiseLvl*s)
        if goodPixMask != None: bkgMask = np.logical_and(bkgMask, goodPixMask)
        m = dataImg[bkgMask].mean()
        s = dataImg[bkgMask].std()
    
    return m, s


def cleanBleeding(img, sigmas = 1.):
    """Fill bled columns with minimum value of image.
        
    Clean vertical bleeding by filling the whole column where bleeding occurs
    with the minimum value of the image.
    'sigmas' is the threshold (in std dev units) to mark a colum as containing bleeding.
    """
    colSums = np.array([np.sum(img[:,i]) for i in range(0,img.shape[1])])
    colSums = colSums - colSums.min()
    blIndx = colSums > sigmas * colSums.std()
    img[:,blIndx] = img.min()


def maskBleeding(scidata, badPixMask = None):
    """Return a mask for the bad and bled columns (True on bleeding).
        
    Identify the columns where bleeding appears (using excess of counts method)
    and return a mask that covers (True on bleeding) the bad columns.
    When supplied a pre-existing badPixMask, use it and
    return the union of both masks.
    """
    
    if badPixMask == None:
        goodPixMask = None
        colSums = scidata.sum(axis=0)

        mm = colSums.mean()
        ss = colSums.std()
        
        colSums = colSums - mm
        bleedIndx = colSums > 2.0 * ss
    
    else:
        goodPixMask = ~badPixMask
        colSums = np.array([acol[aMaskCol].sum() for aCol, aMaskCol in zip(scidata.T, goodPixMask.T)])

        #colNorms[emptyColsMask] = 1.
        colNorms = goodPixMask.sum(axis=0)
        colNorms[colNorms == 0] = 1
        colSums = colSums / colNorms
        
        nonemptyColsMask = (goodPixMask.sum(axis=0) > 0)
        
        mm = colSums[nonemptyColsMask].mean()
        ss = colSums[nonemptyColsMask].std()
        
        colSums[nonemptyColsMask] = colSums[nonemptyColsMask] - mm
        bleedIndx = nonemptyColsMask * (colSums > 2.0 * ss)

    #Widen here the bleeding colums to bborder pixels to each side.
    idx = np.where(bleedIndx == True)[0]
    bborder = 3
    for i in idx:
        for j in range(-bborder, bborder + 1, 1):
            if i + j >= 0 and i + j < bleedIndx.shape[0]:
                bleedIndx[i + j] = True
    
    bleedMask = [bleedIndx] * badPixMask.shape[1]

    return bleedMask


def processCSTARImage(scidata):
    """Helper function that process a CSTAR image to be used in the coadd method.
    
    Create a mask for bled columns, bad pixels, and create a uint8 numpy
    array for the image. Return the uint8 image and the mask.
    """
    
    badPixMask = np.array(scidata < 0.)
    bleedMask = maskBleeding(scidata, badPixMask)
    
    #Add bleeding colums to the bad pixels mask
    badPixMask = np.logical_or(bleedMask, badPixMask)
    goodPixMask = np.logical_not(badPixMask)
    
    #Set bad pixels to zero
    scidata[badPixMask] = 0.
    
    reduced_im = makeSourcesImage(scidata, mask=goodPixMask)
    xmin = reduced_im.min()
    xmax = reduced_im.max()
    reduced_im = (reduced_im - xmin)*(255.0/(xmax - xmin))
    reduced_im = reduced_im.astype('uint8', copy=False)
    
    return reduced_im, badPixMask

################################################################



################################################################
# VISUALIZATION ------------------------------------------------

from matplotlib.colors import LinearSegmentedColormap
import matplotlib.pyplot as plt

_X = np.linspace(0.,1.,num=10)
_beta = 20.
_Y = [math.acosh(_beta*(it + 1./_beta) + 1E-5)/math.acosh(_beta*(_X[-1] + 1./_beta) + 1E-5) for it in _X]
_lvls = tuple(zip(_X, _Y, _Y))
_cdict = {'red': _lvls, 'green': _lvls, 'blue': _lvls}
Acosh = LinearSegmentedColormap('ACosh', _cdict)


def makeSourcesImage(dataImg, noiseLvl = 3., mask = None):
    """Return an image where the background and the given mask, is set to zero.
        
    Background is anything below 'noiseLvl' sigmas.
    """
    
    m, s = bkgNoiseSigma(dataImg, noiseLvl = noiseLvl, goodPixMask = mask)
    
    srcsImg = dataImg.copy()
    if mask != None:
        srcsMask = np.logical_and(mask, dataImg > m + noiseLvl*s)
    else:
        srcsMask = dataImg > m + noiseLvl*s
    srcsImg[np.logical_not(srcsMask)] = 0.
    return srcsImg


def createU8ImageFromFile(aFile):
    """Returns a uint8 image from a file and its bad pixels mask.
        
    Given the path to a CSTAR image, return a uint8 image and a bad pixel
    (bleed included) mask.
    """
    scidata = fits.getdata(aFile).astype('float32')
    im, bpMask = processCSTARImage(scidata)
    return im, bpMask


def makeSourcesGrid(img, sources, border = 10, returnImage = False):
    """Plot on screen a grid with the sources extracted from an image.
        
    Plot on screen a grid with the sources given as an array of [x,y]
    values extracted from img.
    If returnImage is True, return a numpy array to be plotted instead.
    """
    l = len(sources)
    m = int(math.sqrt(l))
    if l == m*m:
        h = m
        w = m
    elif l <= m*(m + 1):
        h = m + 1
        w = m
    else:
        h = m + 1
        w = m + 1
    
    bd = border
    src_grid = np.zeros((h*2*bd, w*2*bd))
    img_border = cv2.copyMakeBorder(img, bd, bd, bd, bd, borderType=cv2.BORDER_CONSTANT, value=0)
    for ind, (xc, yc) in enumerate(sources.astype('int')):
        xc = xc + bd
        yc = yc + bd
        row = 2*bd*(ind / w)
        col = 2*bd*(ind % w)
        src_grid[row:row + 2*bd, col:col + 2*bd] = img_border[yc - bd: yc + bd, xc - bd: xc + bd]
        cv2.rectangle(src_grid, (col, row), (col+2*bd, row+2*bd), color = 255)
    
    if returnImage:
        return src_grid
    else:
        plt.figure(figsize=(10,10))
        plt.imshow(src_grid, cmap = Acosh, interpolation = 'none')
        plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
        plt.show()


def circleSources(img, sources, radius=10, returnImage=False):
    """Plot an image with circles around sources.
    
    Plot on screen the img array with circles around the sources.
    sources is a 2D numpy array with each entry a pair of [x,y] values.
    If returnImage is True, return the numpy array for the image instead.
    """
    img3ch = cv2.merge([img, img, img])
    for aCenter in sources.astype(int):
        cv2.circle(img3ch, tuple(aCenter), radius=radius, color = (0,100,0), thickness=2)

    if returnImage:
        return img3ch
    else:
        plt.figure(figsize=(10,10))
        plt.imshow(img3ch, interpolation = 'none')
        plt.show()


def simpleplot(img):
    """A simple plot call for a quick view of an astronomical image."""
    plt.figure(figsize=(10,10))
    plt.imshow(img, cmap = Acosh, interpolation='none')
    plt.show()

################################################################



################################################################
# ALIGNMENT ----------------------------------------------------


# OBSOLETE, USE FINDSOURCES NOW
#def findCentroids(img):
#"""This works on an unit8 image with background substracted
#    and set to zero. It will find the centroids of all the sources
#    returned as a numpy array of [x,y] values."""
    #Pick only bright stars
#    img1 = img.copy()
#    thresh = 0.3*img.max()
#    img1[img > thresh] = 255
#    img1[img < thresh] = 0
    
    #Make stars bigger
#    krn = np.ones(shape=(5,5))
#    img1 = cv2.dilate(img1,krn)

    #Find contours
#    cnt1, hier1 = cv2.findContours(img1, mode = cv2.RETR_LIST, method = cv2.CHAIN_APPROX_NONE)
    
#    mm = [cv2.moments(cnt1[i]) for i in range(len(cnt1))]
#    centroids = np.array([(mm[i]['m10']/mm[i]['m00'], mm[i]['m01']/mm[i]['m00']) for i in range(len(mm))])
    
#    return centroids


def findSources(img):
    """Return sources sorted by brightness.
        
    img should be an image with background set to zero.
    Anything above zero is considered part of a source.
    """

    img1 = img.copy()
    cnt, hier1 = cv2.findContours(img1, mode = cv2.RETR_LIST, method = cv2.CHAIN_APPROX_SIMPLE)
    img1 = 0

    #This could fail if the enclosing rectangle for one contour intersects
    #  another contour
    #  Improve: set zero outside contour (use drawContours to mask elements outside)
    centroids = []
    brightness = []
    for aCont in cnt:
        #xmin, ymin = map(min, np.squeeze(aCont).T)
        #xmax, ymax = map(max, np.squeeze(aCont).T)

        xy = zip(*[tuple(cc[0]) for cc in aCont])
        xmin = min(xy[0])
        ymin = min(xy[1])
        imext = img[ymin: max(xy[1])+1, xmin:max(xy[0])+1].astype('float32')
        #imext = img[ymin: ymax+1, xmin:xmax+1].astype('float32')

        m01 = sum([(rowind + ymin)*sum(row) for (rowind,row) in enumerate(imext)])
        m10 = sum([(colind + xmin)*sum(col) for (colind,col) in enumerate(imext.T)])
        m00 = np.sum(imext)
        centroids.append([m10/m00, m01/m00])
        brightness.append(m00)

    if len(centroids) < 10:
        print("Warning: Only %d sources found." % (numSrc))

    centroids = np.array(centroids)
    brightness = np.array(brightness)
    bestStars = centroids[np.argsort(-brightness)]

    return bestStars


def findIsolatedSources(img, minRad = 50.):
    """Find sources that are at least minRad pixels from any other.
        
    minRad: the circular radius of the exclusion zone.
    """
    
    centroids = findSources(img)
    
    #This loop makes N^2 operations that can be done in NLgN with a proper data structure (a Kd tree)
    loneStars = []
    for ct in centroids:
        #find neighbor stars
        for ct1 in centroids:
            if np.array_equal(ct1,ct):
                continue
            if np.linalg.norm(ct1 - ct) < minRad:
                break
        
        #if no neighbor found, add centroids to loneStars
        else:
            loneStars.append(ct)
    
    return loneStars


def alignImages(refImgU8, imgU8, approxM = np.array([[1,0,0],[0,1,0]]), refStars = [], minRad = 100.):
    """Find the transformation matrix that takes imgU8 into the reference image refImgU8.
        
    An approximate transformation matrix approxM should be provided to find possible 
    candidates for matching stars.
    refStars is a set of reference stars in the reference image to find companions in imgU8.
    If not provided, they will be found using findSources.
    """

    if len(refStars) == 0:
        refStars = findSources(refStars)
    refStars = refStars[:50]
    
    centroids2 = findSources(imgU8)[:70]
    if len(centroids2) < 4 or len(refStars) < 4:
        raise ValueError("Insufficient sources to estimate a transformation.")

    #rotate centroids in second image
    rotCentroids2 = cv2.transform(np.array([centroids2]), approxM)[0]
    src_pts = []
    dst_pts = []
    for ct in refStars:
        #find corresponding stars
        for (i, ct1) in enumerate(rotCentroids2):
            if np.linalg.norm(ct1 - ct) < minRad:
                src_pts.append(centroids2[i])
                dst_pts.append(ct)

    if len(dst_pts) < 4:
        raise ValueError("Not enough matches found.")

    src_pts = np.array(src_pts)
    dst_pts = np.array(dst_pts)
    try:
        M, mask = findAffineTransform(src_pts, dst_pts)
    except:
        M, mask = blindAlign(refImgU8, imgU8, refStars, minRad)

    return M


def blindAlign(refImgU8, imgU8, refStars = [], minRad = 100.):
    """Brute force try to approximate transformations to register 2 images.
        
    Align two images by trying 36 different rotations as
    the starting approximate transformation to match star pairs.
    It will then pick the one that gave the best fit between the images. 
    This best transformation includes rotation and translation.
    """
    
    if len(refStars) == 0:
        refStars = findSources(refStars)
    refStars = refStars[:50]

    centroids2 = findSources(imgU8)[:70]
    if len(centroids2) < 4 or len(refStars) < 4:
        raise ValueError("Insufficient sources to estimate a transform.")

    bestM = np.zeros(shape=(2,3), dtype='float32')
    bestMask = []
    bestModel = 0
    for alpha in np.arange(0.,360.,10.):
        approxM = cv2.getRotationMatrix2D((imgU8.shape[0]//2, imgU8.shape[1]//2), alpha, 1.0)
        
        rotCentroids2 = cv2.transform(np.array([centroids2]), approxM)[0]
        src_pts = []
        dst_pts = []
        for ct in refStars:
            #find corresponding stars
            for (i, ct1) in enumerate(rotCentroids2):
                if np.linalg.norm(ct1 - ct) < minRad:
                    src_pts.append(centroids2[i])
                    dst_pts.append(ct)
        
        assert(len(src_pts) == len(dst_pts))
        if len(dst_pts) < 4:
            continue
        
        src_pts = np.array(src_pts)
        dst_pts = np.array(dst_pts)
        M, mask = findAffineTransform(src_pts, dst_pts)
        if mask.sum() > bestModel:
            bestM = M
            bestMask = mask

    return bestM, bestMask


def findAffineTransform(src_pts, dst_pts):
    """Find a rotation + translation transformation between pairs of points using a RANSAC algorithm."""
    assert src_pts.shape == dst_pts.shape
    data = np.hstack((src_pts, dst_pts))  #a set of observed data points
    transfModel = LinearTransformModel()  #a model that can be fitted to data points

    minNDataPoints = 2     #the minimum number of data values required to fit the model
    maxPixDist     = 5.    #a threshold value for determining when a data point fits a model
    minNInliers    = 10    #the number of close data values required to assert that a model fits well to data

    #w is the probability of choosing an inlier each time a single point is selected
    #w = number of inliers in data / number of points in data
    w = 45./len(src_pts)
    
    #p is the probability that the RANSAC algorithm in some iteration selects only inliers
    #from the input data set when it chooses the n points from which the model parameters are estimated
    p = 0.99
    
    #the maximum number of iterations allowed in the algorithm
    maxNIterations = 2 * int(math.log(1. - p) / math.log(1. - w ** minNDataPoints))
    if maxNIterations > 1000: maxNIterations = 1000

    mask = np.zeros(len(src_pts), dtype = 'bool')
    try:
        M, maskDict = ransac.ransac(data,
                                    transfModel,
                                    minNDataPoints,
                                    maxNIterations * 2,
                                    maxPixDist,
                                    minNInliers,
                                    debug=False,
                                    return_all=True)
    except:
        M = None

    else:
        mask[maskDict['inliers']] = True
        #improve M with all inliers
        M = transfModel.fit(data[mask])
    
    return M, mask


def getDatetimeFromFile(aFile):
    """Return a datetime object for the observation time from a path to a CSTAR image file."""
    dateStr = fits.getval(aFile, 'DATE-OBS').strip()
    timeStr = fits.getval(aFile, 'TIME').strip()
    t = d.datetime.strptime(" ". join([dateStr, timeStr]), '%Y %b %d %H:%M:%S.%f')
    return t


def coadd(fileNames, refFileName, getStats = None):
    """Align CSTAR images using as reference the image in the file refFileName.
    
    Add them in a single image and create a mask that contains the proportion of
    the maximum sky level added to each pixel (accounts for different sky levels in the added images).
    The mask is zero for pixels where there is no data.
    Return coaddedImg, mask
    """

    #create first a uint8 reference image file from refFileName
    im_ref, bp = createU8ImageFromFile(refFileName)
    #t_ref = getDatetimeFromFile(refFileName)
    
    #Find reference stars in reference image. Only pick 50 brightest (picking many would cause confusion)
    refStars = findSources(im_ref)[:50]

    #initiate co-added image and its mask
    coAddedImg = np.zeros(im_ref.shape, dtype = 'float32')
    flatMask = np.zeros(im_ref.shape, dtype = 'float32')
    
    #Here do a blind search for the rotation between ref image and first image of the set
    imu8, bp = createU8ImageFromFile(fileNames[0])
    M1, mask = blindAlign(im_ref, imu8, refStars)
    alpha0 = math.atan2(M1[1,0], M1[0,0])*180./math.pi

    lastTUsed = getDatetimeFromFile(fileNames[0])
    nimgs = 0
    sumMeans = 0.
    stats = []
    for ffile in fileNames:
        print("Processing %s. %d of %d files." % (ffile, nimgs + 1, len(fileNames)))
        
        #Create uint8 image from file to use to find transformation matrix
        hdulist = fits.open(ffile)
        hd = hdulist[0].header
        t2 = d.datetime.strptime(" ".join([hd['DATE-OBS'].strip(), hd['TIME'].strip()]), '%Y %b %d %H:%M:%S.%f')
        mm = hd['SKYM']
        scidata = (hdulist[0].data).astype('float32')
        hdulist.close()
        imu8, bp = processCSTARImage(scidata)
        
        #Create flat mask with image noise level in good pixels (zero outside)
        flat = np.ones(imu8.shape, dtype='float32')
        flat[bp] = 0.
        flat = flat * mm
        sumMeans += mm
    
        alpha = alpha0 + 360.*(t2 - lastTUsed).seconds/(24.*60.*60.)
        Mapp = cv2.getRotationMatrix2D((imu8.shape[0]//2,imu8.shape[1]//2), -alpha, 1.0)
        M = alignImages(im_ref, imu8, Mapp, refStars)
        noisy_im_rot = cv2.warpAffine(scidata, M, scidata.shape, borderMode = cv2.BORDER_CONSTANT, borderValue = 0.)
        flat_rot = cv2.warpAffine(flat, M, flat.shape, borderMode = cv2.BORDER_CONSTANT, borderValue = 0.)
    
        coAddedImg = coAddedImg + noisy_im_rot
        flatMask = flatMask + flat_rot
        nimgs = nimgs + 1
    
        if getStats != None:
            flatMask2Divide = flatMask.copy()
            flatMask2Divide = flatMask2Divide / sumMeans
            flatMask2Divide[flatMask == 0] = 1.
            imgf = coAddedImg.copy()
            imgf = imgf / (flatMask2Divide * nimgs)
            flatMask2Divide = 0
            stats.append(getStats(imgf, flatMask.astype('bool')))
            imgf = 0

    flatMask = flatMask / sumMeans
    flatMask2Divide = flatMask.copy()
    flatMask2Divide[flatMask == 0] = 1.
    coAddedImg = coAddedImg / (flatMask2Divide * nimgs)
    flatMask2Divide = 0

    if getStats == None:
        return coAddedImg, flatMask
    else:
        return coAddedImg, flatMask, stats


class LinearTransformModel:
    def __init__(self): pass
    def fit(self, data):
        A = []
        y = []
        for pt in data:
            x1 = pt[0]
            x2 = pt[1]
            A.append([x1, x2, 1, 0])
            A.append([x2, -x1, 0, 1])
            y.append(pt[2])
            y.append(pt[3])
        
        A = np.matrix(A)
        y = np.matrix(y)
        
        sol, resid, rank, sv = np.linalg.lstsq(A,y.T)
        c = sol.item(0)
        s = sol.item(1)
        t1 = sol.item(2)
        t2 = sol.item(3)
        approxM = np.array([[c, s, t1],[-s, c, t2]])
        return approxM
    
    def get_error(self, data, approxM):
        error = []
        for pt in data:
            spt = pt[:2]
            dpt = pt[2:]
            dpt_fit = cv2.transform(np.array([[spt]]), approxM)[0]
            error.append(np.linalg.norm(dpt - dpt_fit))
        return np.array(error)

################################################################
# PSF Fitting --------------------------------------------------

from scipy import signal

def __gauss(shape = (10,10), center=None, sx=2, sy=2):
    h, w = shape
    if center is None: center = ((h-1)/2., (w-1)/2.)
    x0,y0 = center
    x,y = np.meshgrid(range(w),range(h))
    norm = math.sqrt(2*math.pi*(sx**2)*(sy**2))
    return np.exp(-0.5*((x-x0)**2/sx**2 + (y-y0)**2/sy**2))/norm


def __getCVectors(refImage, kernelShape = (10,10), gaussList = None, modBkgDeg = 2):
    #degMod is the degree of the modulating polyomial
    kh, kw = kernelShape
    if gaussList is None: gaussList = [{'sx':2., 'sy':2.}]
    
    v,u = np.mgrid[:kh,:kw]
    C = []
    for aGauss in gaussList:
        if 'modPolyDeg' in aGauss: degMod = aGauss['modPolyDeg']
        else: degMod = 2
        
        allUs = [pow(u,i) for i in range(degMod + 1)]
        allVs = [pow(v,i) for i in range(degMod + 1)]
        
        if 'center' in aGauss: center = aGauss['center']
        else: center = None
        gaussK = __gauss(shape=(kh,kw), center=center, sx=aGauss['sx'], sy=aGauss['sy'])
        
        C.extend([signal.convolve2d(refImage, gaussK * aU * aV, mode='same') \
                  for i, aU in enumerate(allUs) for aV in allVs[:degMod+1-i]])
    
    #finally add here the background variation coefficients:
    h, w = refImage.shape
    y, x = np.mgrid[:h,:w]
    allXs = [pow(x,i) for i in range(modBkgDeg + 1)]
    allYs = [pow(y,i) for i in range(modBkgDeg + 1)]
    C.extend([anX * aY for i, anX in enumerate(allXs) for aY in allYs[:modBkgDeg+1-i]])
    return C


def __coeffsToKernel(coeffs, gaussList, kernelShape = (10,10)):
    kh, kw = kernelShape
    
    v,u = np.mgrid[:kh,:kw]
    
    kernel = np.zeros((kh,kw))
    for aGauss in gaussList:
        if 'modPolyDeg' in aGauss: degMod = aGauss['modPolyDeg']
        else: degMod = 2
        
        allUs = [pow(u,i) for i in range(degMod + 1)]
        allVs = [pow(v,i) for i in range(degMod + 1)]
        
        if 'center' in aGauss: center = aGauss['center']
        else: center = None
        gaussK = __gauss(shape=kernelShape, center=center, sx=aGauss['sx'], sy=aGauss['sy'])
        
        ind = 0
        for i, aU in enumerate(allUs):
            for aV in allVs[:degMod+1-i]:
                kernel += coeffs[ind] * aU * aV
                ind += 1
        kernel *= gaussK

    #kernel /= kernel.sum()
    
    return kernel


def __coeffsToBackground(shape, coeffs, bkgDeg = None):
    if bkgDeg is None: bkgDeg = int(-1.5 + 0.5*math.sqrt(9 + 8*(len(coeffs) - 1)))

    h, w = shape
    y, x = np.mgrid[:h,:w]
    allXs = [pow(x,i) for i in range(bkgDeg + 1)]
    allYs = [pow(y,i) for i in range(bkgDeg + 1)]

    mybkg = np.zeros(shape)

    ind = 0
    for i, anX in enumerate(allXs):
        for aY in allYs[:bkgDeg+1-i]:
            mybkg += coeffs[ind] * anX * aY
            ind += 1

    return mybkg


def getOptimalKernelAndBkg(image, refImage, gaussList, bkgDegree, kernelShape=(10,10)):
    """Do Optimal Image Subtraction and return optimal kernel and background.
        
    This is an implementation of the Optimal Image Subtraction algorith of Alard&Lupton.
    It returns the best kernel and background fit that match the two images.
    
    gaussList is a list of dictionaries containing data of the gaussians used in the decomposition
    of the kernel.
    
    bkgDegree is the degree of the polynomial to fit the background.
    
    kernelShape is the shape of the kernel to use.
    
    To subtract, do: image - (refImage (*) kernel + background) 
    where (*) denotes convolution.
    """
    C = __getCVectors(refImage, kernelShape, gaussList, bkgDegree)
    m = np.array([[(ci*cj).sum() for ci in C] for cj in C])
    b = np.array([(image*ci).sum() for ci in C])
    coeffs = np.linalg.solve(m,b)
    
    #nKCoeffs is the number of coefficients related to the kernel fit, not the background fit
    nKCoeffs = 0
    for aGauss in gaussList:
        if 'modPolyDeg' in aGauss: degMod = aGauss['modPolyDeg']
        else: degMod = 2
        nKCoeffs += (degMod + 1)*(degMod + 2)//2
    
    kernel = __coeffsToKernel(coeffs[:nKCoeffs], gaussList, kernelShape)
    background = __coeffsToBackground(image.shape, coeffs[nKCoeffs:])

    return kernel, background, coeffs






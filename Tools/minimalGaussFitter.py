"""
    # minimalGaussFitter.py
    
    ## Simple 2D Gaussian fitter to an image.
    
    gaussfit(data, params, err): Will estimate the missing parameters of the 
        Gaussian in the params dictionary and use that estimation to find the
        optimum Gaussian that fits data.
    
    Martin Beroiz - 2014
    University of Texas at San Antonio
    email: <martinberoiz@phys.utb.edu>
"""

import math
import numpy as np
from numpy.ma import median
from scipy import optimize, stats
import cv2

__version__ = '0.1'

def gaussfit(data, params = {}, err = None):
    """Fit a Gaussian to data (a 2D numpy array).
        
        'params' is a dictionary of Gaussian parameters approximate to the best fit.
        If a parameter is missing it will estimate it using data.
        
        The parameters dictionary keys are: noiseLevel, amplitude, centerX, centerY, sigma1, sigma2, rotAngle.
        
        Return best fit parameters array.
        """
    
    #Fill in the missing supplied moments with estimated ones from moments
    estimateMoments(data, params)
    
    parray = dictToArray(params)
    
    if err is None:
        errorfunction = lambda p: np.ravel((twodgaussian(parray)\
                                            (*np.indices(data.shape)) - data))
    else:
        errorfunction = lambda p: np.ravel((twodgaussian(parray)\
                                            (*np.indices(data.shape)) - data)/err)
    
    p, cov, infodict, errmsg, success = optimize.leastsq(errorfunction, parray, full_output = 1)
    #mp = mpfit(mpfitfun(data,err),parinfo=parinfo,quiet=quiet)
    
    return p, cov, infodict, errmsg, success


def estimateMoments(img, params):
    """Estimate the missing parameters of the Gaussian using img.
        
    The parameters dictionary keys are: noiseLevel, amplitude, centerX, centerY, sigma1, sigma2, rotAngle.
    """

    #all_keys = ['amplitude', 'noiseLevel', 'sigma1', 'sigma2', 'rotAngle', 'centerX', 'centerY']
    if 'noiseLevel' not in params:
        #estimate noiseLevel
        params['noiseLevel'] = np.median(img.ravel())
    
    if {'centerX', 'centerY', 'amplitude', 'rotAngle', 'sigma1', 'sigma2'}.issubset(params):
        return
    
    #estimate center
    #indx is an array whose element [i,j] is the array [i,j]
    indx = np.indices(img.shape).swapaxes(0,2).swapaxes(0,1)
    thresh = 0.1 * (img.max() - params['noiseLevel']) + params['noiseLevel']
    #thresh = 18.
    goodind = indx[img > thresh]
    covar, mean = cv2.calcCovarMatrix(goodind.astype('float32'), cv2.COVAR_SCALE | \
                                                                 cv2.COVAR_ROWS  | \
                                                                 cv2.COVAR_SCRAMBLED)
    mean = mean[0]
    if 'centerX' not in params:
        params['centerX'] = mean[1]
    if 'centerY' not in params:
        params['centerY'] = mean[0]
    if not 'amplitude' in params:
        params['amplitude'] = img[tuple(mean.astype(int))] - params['noiseLevel']
                                      
    if not {'rotAngle', 'sigma1', 'sigma2'}.issubset(params):
        eVal, eVec = cv2.eigen(covar, computeEigenvectors=True)[1:]
        # Conversion + normalisation required due to 'scrambled' mode
        eVec = cv2.gemm(eVec, goodind.astype('float32') - mean, 1, None, 0)[:2]
        # apply_along_axis() slices 1D rows, but normalize() returns 4x1 vectors
        eVec = np.apply_along_axis(lambda n: cv2.normalize(n).flat, 1, eVec)
                                          
        eVal = eVal[:2].flatten()
                                          
        if not 'sigma1' in params:
            params['sigma1'] = math.sqrt(eVal[0])
        if not 'sigma2' in params:
            params['sigma2'] = math.sqrt(eVal[1])
        if not 'rotAngle' in params:
            params['rotAngle'] = math.atan2(eVec[0][0], eVec[0][1])
                                      
    return


def twodgaussian(params):
    """Return a function that calculates the value of a Gaussian with params at a certain row, column."""
    
    nl  = params[0] #['noiseLevel']
    A   = params[1] #['amplitude']
    mux = params[2] #['centerX']
    muy = params[3] #['centerY']
    
    sx  = params[4] #['sigma1']
    sy  = params[5] #['sigma2']
    
    alpha = params[6] #['rotAngle']
    
    def rotgauss(row,col):
        x = col
        y = row
        xp = (x - mux) * np.cos(alpha) + (y - muy) * np.sin(alpha)
        yp = -(x - mux) * np.sin(alpha) + (y - muy) * np.cos(alpha)
        g = nl + A*np.exp(-0.5*((xp / sx)**2 + (yp / sy)**2))
        
        return g
    
    return rotgauss


def dictToArray(paramsDict):
    """Transform the params dictionary to an array"""
    p = np.empty(7)
    p[0] = paramsDict['noiseLevel']
    p[1] = paramsDict['amplitude']
    p[2] = paramsDict['centerX']
    p[3] = paramsDict['centerY']
    p[4] = paramsDict['sigma1']
    p[5] = paramsDict['sigma2']
    p[6] = paramsDict['rotAngle']
    return p


def arrayToDict(p):
    """Transform the params array to a dictionary"""
    paramsDict = {}
    paramsDict['noiseLevel'] = p[0]
    paramsDict['amplitude'] = p[1]
    paramsDict['centerX'] = p[2]
    paramsDict['centerY'] = p[3]
    paramsDict['sigma1'] = p[4]
    paramsDict['sigma2'] = p[5]
    paramsDict['rotAngle'] = p[6]
    return paramsDict


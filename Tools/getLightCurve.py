"""
    # getLightCurve.py
    
    Scan rootDir looking for FITS files and perform aperture photometry on them.
    This supposes all the images are aligned so it converts RA, Dec to pixel only once.
    
    Usage:
    python getLightCurve.py [options args] rootDir outputFile
    Options:
    -v, --version: Prints the version of this program and exits.
    -h, --help:    Prints this help page and exits.
    -r, --ra:      The RA of the source to search for (in degrees).
    -d, --dec:     The Dec of the source to search for (in degrees).
    rootDir:       Set the root directory that contains all the FITS files to be processed.
    outputFile:    Set the file on which the light curve table will be saved to.

    Martin Beroiz - June 2014
    <martinberoiz@phys.utb.edu>
    University of Texas at San Antonio
    """

import os
import getopt
import sys
from astropy.io import fits
from astropy.io import ascii
from astropy import wcs
from astropy.time import Time
import datetime as d
import numpy as np
import math
from photutils import CircularAperture, aperture_photometry

__version__ = '0.1'

def getFlux(hdulist, ra, dec):
    """Get the aperture photometry flux."""
    head       = hdulist[0].header
    image      = hdulist[0].data
    image_mask = hdulist[1].data.astype('bool')
    bkg_level  = head['skym']
    magzero    = head['magzero']
    date       = head['date-obs']
    the_wcs    = wcs.WCS(head)
    apRadius   = 3.5
    
    try:
        pixPoint = the_wcs.all_world2pix([[ra, dec]],0,tolerance=1E-3)[0]
        x,y = pixPoint.astype('int')
        h, w = image.shape
        if(x > w or x < 0 or y < 0 or y > h):
            #print("pixel %d %d is out of bounds." % (x,y))
            return None
        elif(image_mask[y,x] == False):
            #print("Pixel fell on mask.")
            return None
        else:
            apertures = CircularAperture([[x,y]], apRadius)
            flux = aperture_photometry(image, apertures)[0]
            flux -= bkg_level * np.pi * apRadius**2
            return (date, flux, magzero, -2.5*np.log10(flux) + magzero)
    except:
        #print("Try block didn't work.")
        return None


def scanThruFiles(init_path, outputFile, ra, dec):
    """Scan through init_path and call getFlux on each FITS file found."""
    listing = os.listdir(init_path)
    listing = [os.path.join(init_path, anitem) for anitem in listing
               if anitem.endswith('.fits') or os.path.isdir(os.path.join(init_path, anitem))]
    listing.sort()
    for infile in listing:
        if os.path.isdir(infile):
            print('Getting into folder: %s' % (os.path.basename(infile)))
            scanThruFiles(infile, outputFile, ra, dec)
            outputFile.flush()
        else:
            #Do work here
            print('working with file %s' % (os.path.basename(infile)))
            hdulist = fits.open(infile)
            params = getFlux(hdulist, ra, dec)
            if params is not None:
                date, flux, magzero, mag = params
                outputFile.write("%s, %f, %f, %f\n" % (date, flux, magzero, mag))
            hdulist.close()


if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvr:d:", ["help","version","ra=","dec="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    ra = None
    dec = None
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(__doc__)
            sys.exit(2)
        if opt in ('-v', '--version'):
            print('Version: ' + __version__)
            sys.exit(2)
        if opt in ('-r', '--ra'):
            ra = np.double(arg)
        if opt in ('-d', '--dec'):
            dec = np.double(arg)

    if (ra is None or dec is None):
        usage()
        sys.exit(1)

    root_dir = args[0]
    outputFile = open(args[1], "w")
    outputFile.write('# Light curve for source at RA: %f Dec: %f\n' % (ra, dec))
    outputFile.write('#Date (ISOT format), Flux (counts), MAGZERO, Magnitud (i)')
    scanThruFiles(root_dir, outputFile, ra, dec)
    outputFile.close()


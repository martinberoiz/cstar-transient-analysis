"""
   # generateStableCSTAR.py
   
   ## Coadd CSTAR images and save them to a FITS file. It will also generate a text
   ## file containing the names of the files used in the addition. The name has
   
   Usage: generateStableCSTAR.py [options] -r,--ref <referenceFile> -o <outDirectory> <filesToCoadd>
   
   ### Keywords:
   -r, --ref: Reference file to which all images will be aligned.
   -o       : Directory on which images will be saved.
   
   ### Options:
   -i, --fileid: The output files will be serialized consecutively starting from this number.
   -h, --help: Prints this help and exits.
   -v, --version: Prints version information.
    
   Martin Beroiz - 2014
   <martinberoiz@phys.utb.edu>
   University of Texas at San Antonio
"""

__version__ = '0.1'

import imgToolbox as itmb
import datetime as d
from astropy.io import fits
import os
import getopt
import sys

def coaddAndSave(thefiles, refFile, fileid, outdir = './'):
    """This will align, coadd and save a group of CSTAR images to a FITS file.
        
        Arguments:
        thefiles: the files in the group to be coadded and saved.
        refFile: The reference file to be used as a reference in the alignment.
        fileid: The counter ID number in the serialization of the output file names.
        outdir: The output directory to save the file to.
        """
    #bestdayfile = '/Users/martin/Academia/Research/TOROS/CSTAR2010/data1/phot/806/dir2/16D00101.fits.gz'
    coadded, mask = itmb.coadd(thefiles, refFile)
    
    firsthead = fits.getheader(thefiles[0])
    imghdu = fits.PrimaryHDU(coadded)
    imghead = imghdu.header
    
    #Complete the header information for the file
    imghead['RA'] = 20.168
    imghead['DEC'] = -89.9
    skym, skys = itmb.bkgNoiseSigma(coadded, goodPixMask = mask.astype('bool'))
    imghead['SKYM'] = skym
    imghead['SKYS'] = skys
    texp = 0.
    for afile in thefiles:
        texp += fits.getval(afile, 'EXPOSURE')
    imghead['EXPOSURE'] = (texp, 'summation of expousure time in all files added.')
    imghead['DATE-OBS'] = (firsthead['DATE-OBS'], 'Date of first image added.')
    imghead['TIME'] = (firsthead['TIME'], 'startUT of first image added.')
    imghead['FILTER']  = firsthead['FILTER']
    
    #set out file names
    txtoutname = ('files_%05d.txt' % (fileid))
    fitsoutname = ('image_%05d.fits' % (fileid))
    
    imghead['COMMENT'] = ('This is a coadded image of %d images described in %s file' % (len(thefiles), txtoutname))
    imghead['COMMENT'] = ('using as reference image the first image from Jun 13 2010 (16D00101).')
    
    #write text file with the list of files used in the addition
    outtxtfile = open(os.path.join(outdir, txtoutname), 'w')
    for afile in thefiles:
        relpath = os.path.join(*thefiles[0].split(os.path.sep)[-6:])
        outtxtfile.write("%s\n" % (relpath))
    outtxtfile.close()
    
    #Create a secondary HDU with the mask
    mskhdu = fits.ImageHDU(mask)
    mskhdu.header['COMMENT'] = 'This is a mask (zero for bad pixels) for the image in the primary HDU.'
    mskhdu.header['COMMENT'] = 'The value of each pixel is the proportion of sky noise level on that pixel.'
    
    #write the FITS file to disk
    newhdulist = fits.HDUList([imghdu, mskhdu])
    newhdulist.writeto(os.path.join(outdir, fitsoutname), clobber = True)
    
    return


def groupGoodFiles(filenames,
                   refFile = None,
                   fileid = 0,
                   outdir = './',
                   maxSeparation = 40,
                   vetoNoiseLvl = 6000,
                   maxNumberOfFiles = 21,
                   minNumberOfFiles = 20):
    """This will look for a group of files that are usable and then call coaddAndSave on that group.
    
    Each group of files will represent a coadded FITS file.
    
    Arguments:
        filenames: All the file names to be processed.
        refFile: The reference file to be used as a reference in the alignment.
        fileid: The counter ID number in the serialization of the output file names.
        outdir: The output directory to save the file to.
        maxSeparation: The maximum separation between images in minutes.
        vetoNoiseLvl: The maximum noise level for a file to be considered good.
        maxNumberOfFiles: The group is rejected if the number of files is larger than this number.
        minNumberOfFiles: The group is rejected if the number of files is smaller than this number.
    """

    if refFile == None: refFile = filenames[0]
    goodFiles = []
    nimgs = 0
    initDate = None
    for afile in filenames:
        print("Processing %s. %d of %d files." % (afile, nimgs + 1, len(filenames)))
        hdulist = fits.open(afile)
        hd = hdulist[0].header
        newDate = d.datetime.strptime(" ". join([hd['DATE-OBS'].strip(), hd['TIME'].strip()]), '%Y %b %d %H:%M:%S.%f')
        skyLevel = hd['SKYM']
        
        if skyLevel < vetoNoiseLvl:
            if initDate == None: initDate = newDate
            p1 = (newDate - initDate) < d.timedelta(minutes = maxSeparation)
            p2 = len(goodFiles) <= maxNumberOfFiles
            if p1 and p2:
                goodFiles.append(afile)
            else:
                if len(goodFiles) >= minNumberOfFiles:
                    #Process the files (add them up and output FITS file)
                    coaddAndSave(goodFiles, refFile, fileid, outdir)
                    fileid += 1
                goodFiles = []
                initDate = None
        
        nimgs += 1

    if len(goodFiles) != 0 and len(goodFiles) > minNumberOfFiles:
        #Process here remaining files
        coaddAndSave(goodFiles, refFile, nimgs, outdir)
        fileid += 1
        goodFiles = []

    return

def usage():
    print __doc__

def version():
    print "Version %s" % (__version__)
    print "Martin Beroiz - 2014\nmartinberoiz@phys.utb.edu\nUniversity of Texas at San Antonio"

if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hvi:r:o:", ["help", "version", "fileid=", "ref="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    fileid = 0
    refFile = None
    outdir = None
    for opt, arg in opts:
        if opt == '-o':
            outdir = arg
        if opt in ('-h', '--help'):
            usage()
            sys.exit(2)
        if opt in ('-v', '--version'):
            version()
            sys.exit(2)
        if opt in ('-i', '--fileid'):
            fileid = int(arg)
        if opt in ('-r', '--ref'):
            refFile = arg

    sources = args
    sources.sort()
    groupGoodFiles(sources, refFile, fileid, outdir)


